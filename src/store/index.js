import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    items: []
  },
  mutations: {
    setItems(state, payload) {
      state.items = Vue.set(state, 'items', payload);
    }
  },
  actions: {//call to backend
    fetchItems(context, payload) {
      payload = getData();
      context.commit('setItems', payload);
    }//context => istanza dello store iniettata
  },
  modules: {
  }
});

function getData(){
  return [
    {id: 0, title: "todo 1", state: "todo"},
    {id: 1, title: "todo 2", state: "todo"},
    {id: 2, title: "todo 3", state: "done"}
    ]
}
